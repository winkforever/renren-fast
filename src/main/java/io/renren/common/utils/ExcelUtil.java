package io.renren.common.utils;

import com.alibaba.excel.EasyExcel;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

/**
 * @author linhaiwei
 * @date 2022/1/4 16:45
 * @description: excel工具类
 */
public class ExcelUtil {

    public static <T> List<T> readExcel(MultipartFile excel, Class clazz) {
        ExcelListener excelListener = new ExcelListener();
        try {
            EasyExcel.read(excel.getInputStream(), clazz, excelListener).sheet().doRead();
        } catch (IOException ie) {
            ie.printStackTrace();
        }

        return excelListener.getData();
    }
}
