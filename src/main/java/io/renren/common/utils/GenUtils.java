package io.renren.common.utils;

import io.renren.common.exception.RRException;
import io.renren.modules.code.model.CodeDTO;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.util.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * @author linhaiwei
 * @date 2022/2/10 16:28
 * @description: 根据模板生成代码工具类
 */
public class GenUtils {

    public static void generatorCode(ZipOutputStream zip, List<CodeDTO> datas) {
        //设置velocity资源加载器
        Properties prop = new Properties();
        prop.put("file.resource.loader.class", "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
        Velocity.init(prop);
        //封装模板数据
        Map<String, Object> map = new HashMap<>();
        map.put("controllerName", "Test");
        map.put("datas", datas);
        VelocityContext context = new VelocityContext(map);
        //获取模板列表
        List<String> templates = getTemplates();
        for (String template : templates) {
            //渲染模板
            StringWriter sw = new StringWriter();
            Template tpl = Velocity.getTemplate(template, "UTF-8");
            tpl.merge(context, sw);
            try {
                //添加到zip
                zip.putNextEntry(new ZipEntry(getFileName(template, "Test", null, null)));
                IOUtils.write(sw.toString(), zip, "UTF-8");
                IOUtils.closeQuietly(sw);
                zip.closeEntry();
            } catch (IOException e) {
                throw new RRException("渲染模板失败", e);
            }
        }
    }

    /**
     * 获取文件名
     */
    public static String getFileName(String template, String className, String packageName, String moduleName) {
        String packagePath = "";
        if (StringUtils.isNotBlank(packageName)) {
            packagePath += packageName.replace(".", File.separator) + File.separator + moduleName + File.separator;
        }
        if (template.contains("Controller.java.vm")) {
            return packagePath + "controller" + File.separator + className + "Controller.java";
        } else if (template.contains("Service.java.vm")) {
            return packagePath + "service" + File.separator + className + "Service.java";
        } else if (template.contains("ServiceImpl.java.vm")) {
            return packagePath + "serviceImpl" + File.separator + className + "ServiceImpl.java";
        } else if (template.contains("qianxin.properties.vm")) {
            return packagePath + "properties" + File.separator + className + ".properties";
        }
        return null;
    }

    /**
     * 获取需要渲染的模板
     *
     * @date 2022/2/10 16:40
     **/
    public static List<String> getTemplates() {
        List<String> templates = new ArrayList<String>();
        templates.add("template/Controller.java.vm");
        templates.add("template/Service.java.vm");
        templates.add("template/ServiceImpl.java.vm");
        templates.add("template/qianxin.properties.vm");
        return templates;
    }

}
