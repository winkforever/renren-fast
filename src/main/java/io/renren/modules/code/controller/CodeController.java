package io.renren.modules.code.controller;

import io.renren.common.utils.ExcelUtil;
import io.renren.common.utils.GenUtils;
import io.renren.common.utils.UnicodeUtil;
import io.renren.modules.code.model.CodeDTO;
import org.apache.commons.io.IOUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.zip.ZipOutputStream;

/**
 * @author linhaiwei
 * @date 2022/2/10 17:07
 * @description: 代码生成控制器类
 */
@RestController
@RequestMapping("/code")
public class CodeController {

    /**
     * 生成代码
     */
    @RequestMapping("/make")
    public void code(HttpServletResponse response, @RequestParam(value = "file") MultipartFile file) throws IOException {
        List<CodeDTO> datas = ExcelUtil.readExcel(file, CodeDTO.class);
        for (CodeDTO codeDTO : datas) {
            codeDTO.setName(UnicodeUtil.gbEncoding(codeDTO.getDescription()));
        }
        System.out.println("开始生成代码");
        byte[] data = generatorCode(datas);
        response.reset();
        response.setHeader("Content-Disposition", "attachment; filename=\"code.zip\"");
        response.addHeader("Content-Length", "" + data.length);
        response.setContentType("application/octet-stream; charset=UTF-8");
        IOUtils.write(data, response.getOutputStream());
    }

    public byte[] generatorCode(List<CodeDTO> datas) {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        ZipOutputStream zip = new ZipOutputStream(outputStream);
        //生成代码
        GenUtils.generatorCode(zip, datas);
        IOUtils.closeQuietly(zip);
        return outputStream.toByteArray();
    }
}
