package io.renren.modules.code.model;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

/**
 * @author linhaiwei
 * @date 2022/2/11 10:14
 * @description: 生成代码对应类
 */
@Data
public class CodeDTO {

    @ExcelProperty(index = 0)
    private String description;

    @ExcelProperty(index = 1)
    private String methodName;

    @ExcelProperty(index = 2)
    private String service;

    private String name;

    @ExcelProperty(index = 3)
    private String url;

    @ExcelProperty(index = 4)
    private String requestMethod;

    @ExcelProperty(index = 5)
    private String contentType;
}
