package io.renren.modules.user.entity;

import lombok.Data;
import org.springframework.data.annotation.Id;

import java.io.Serializable;

/**
 * @author linhaiwei
 * @date 2021/11/1 16:34
 * @description: TODO
 */
@Data
public class User implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    private String mongodbId;

    private int id;

    private String name;

    private int age;

}
