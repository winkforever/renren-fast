package io.renren.modules.user.controller;

import io.renren.modules.user.entity.User;
import io.renren.modules.user.service.impl.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author linhaiwei
 * @date 2021/11/1 16:41
 * @description: TODO
 */
@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserServiceImpl userService;

    /**
     * 查询所有
     **/
    @GetMapping("/list")
    public List<User> list() {
        return userService.findAllUser();
    }

    /**
     * 添加
     **/
    @PostMapping("/add")
    public void add(@RequestBody User user) {
        userService.insert(user);
    }

    /**
     * 更新
     **/
    @PostMapping("/update")
    public void update(@RequestBody User user) {
        userService.update(user);
    }

    /**
     * 删除
     **/
    @PostMapping("/delete")
    public void delete(@RequestParam("id") String id) {
        userService.delete(id);
    }
}
