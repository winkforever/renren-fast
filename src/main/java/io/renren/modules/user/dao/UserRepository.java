package io.renren.modules.user.dao;

import io.renren.modules.user.entity.User;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * @author linhaiwei
 * @date 2021/11/1 16:37
 * @description: TODO
 */
@Configuration
public interface UserRepository extends MongoRepository<User, String> {
}

