package io.renren.modules.user.service.impl;

import io.renren.modules.user.dao.UserRepository;
import io.renren.modules.user.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author linhaiwei
 * @date 2021/11/1 16:38
 * @description: TODO
 */
@Service
public class UserServiceImpl {

    @Autowired
    private UserRepository userRepository;

    // 查询所有
    public List<User> findAllUser() {
        return userRepository.findAll();
    }

    // 添加
    public void insert(User user) {
        userRepository.insert(user);
    }

    // 删除
    public void delete(String id) {
        userRepository.deleteById(id);
    }

    // 修改
    public void update(User user) {
        userRepository.save(user);
    }
}
