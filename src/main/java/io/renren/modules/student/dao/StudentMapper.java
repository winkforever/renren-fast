package io.renren.modules.student.dao;

import io.renren.modules.student.entity.Student;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * @author linhaiwei
 * @date 2021/12/17 14:49
 * @description: TODO
 */
@Mapper
public interface StudentMapper {


    void updateStudent(@Param("student") Student student);

    Student queryStudentBySno(@Param("sno") String sno);

    void deleteStudentBySno(@Param("sno") String sno);
}
