package io.renren.modules.student.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * @author linhaiwei
 * @date 2021/12/17 14:51
 * @description: TODO
 */
@Data
public class Student implements Serializable {

    private Long id;

    private String sno;

    private String name;

}
