package io.renren.modules.tamper;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author linhaiwei
 * @date 2021/11/5 14:08
 * @description: 测试篡改
 */
@RestController
@RequestMapping("/tamper")
public class TamperController {

    private String test;

    @PostMapping("/addData")
    public String addData(String input) {
        test = input;
        return "调用成功！";
    }

    @GetMapping("/findData")
    public String findData(String input) {
        return input;
    }
}
